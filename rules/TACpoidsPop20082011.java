/*
 * #%L
 * IsisFish data
 * %%
 * Copyright (C) 2006 - 2018 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * #L%
 */
package rules;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixIterator;
import org.nuiton.math.matrix.MatrixND;

import resultinfos.MatrixDiscardsPerStrMetPerZonePop;
import resultinfos.MatrixNoActivity;
import scripts.RuleUtil;
import scripts.SiMatrix;
import fr.ifremer.isisfish.entities.EffortDescription;
import fr.ifremer.isisfish.entities.Metier;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.Strategy;
import fr.ifremer.isisfish.entities.StrategyMonthInfo;
import fr.ifremer.isisfish.entities.TargetSpecies;
import fr.ifremer.isisfish.rule.AbstractRule;
import fr.ifremer.isisfish.simulator.MetierMonitor;
import fr.ifremer.isisfish.simulator.PopulationMonitor;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.util.Doc;

/**
 * TAC peut-etre utilise pour les differents TAC, en proportion des effectifs
 * et/ou avec survie ou non.
 * 
 * <li>Pour utiliser le tac proportionnel, il faut mettre dans le parametre
 * propTac une valeur > 0, le TAC sera alors recalcule a chaque mois de janvier.
 * <li>Pour utiliser la survie il faut mettre dans le parametre propSurvie une
 * valeur > 0, automatiquement les suvie seront ajoute aux effectifs
 * 
 * Created: 7 septembre 2006
 *
 * @author anonymous <anonymous@labs.libre-entreprise.org>
 * @version $Revision: 1.3 $
 *
 * Last update: $Date: 13/12/2012 $
 * by : $Author: Loic $
 */
public class TACpoidsPop20082011 extends AbstractRule {

    /** to use log facility, just put in your code: log.info("..."); */
    static private Log log = LogFactory.getLog(TACpoidsPop20082011.class);

    @Doc("Affected population")
    public Population param_population = null;
    
    @Doc("Proportion de survie")
    public double param_propSurvie = 0;
    

    /** TAC in tonnes */
    
	@Doc("TAC in tons 2008")
	public double param_tacInTons2008 = 4000;
	@Doc("TAC in tons 2009")
	public double param_tacInTons2009 = 3500;
	@Doc("TAC in tons 2010")
	public double param_tacInTons2010 = 3000;
			
    boolean affectation = true;
		
    protected String[] necessaryResult = {
        // put here all necessary result for this rule
        // example:
        // MatrixBiomass.NAME,
        // MatrixNetValueOfLandingsPerStrategyMet.NAME
    };

    /**
     * @return the necessaryResult
     */
    @Override
    public String[] getNecessaryResult() {
        return this.necessaryResult;
    }

    /**
     * Permet d'afficher a l'utilisateur une aide sur la regle.
     * 
     * @return L'aide ou la description de la regle
     */
    @Override
    public String getDescription() {
        return "TAC weight in tons.\nIf you want survival discard use propSurvie other than 0.\nIf you wish TAC computed as a proportion of the biomass use propTac other than 0.";
    }

    /**
     * Appele au demarrage de la simulation, cette methode permet d'initialiser
     * des valeurs
     * 
     * @param context La simulation pour lequel on utilise cette regle
     */
    @Override
    public void init(SimulationContext context) throws Exception {
    }

    /**
     * La condition qui doit etre vrai pour faire les actions.
     * 
     * @param context la simulation pour lequel on utilise cette regle
     * @param step le pas de temps courant
     * @param metier le metier concerne
     * @return vrai si on souhaite que les actions soit faites
     */
    @Override
    public boolean condition(SimulationContext context, TimeStep step, Metier metier)
            throws Exception {

        log.info("test si TAC atteint");
        
		//param_population = (Population) context.getDB().findByTopiaId(param_population.getTopiaId());
		
        double tacInTons = 0;
        boolean result = false;
        if (step.getYear()>2) {
            result = false;
        } else {
			if (step.getYear()==0) {
				tacInTons = param_tacInTons2008;
			} else if (step.getYear()==1) {
				tacInTons = param_tacInTons2009;
			} else if (step.getYear()==2) {
				tacInTons = param_tacInTons2010;
				context.setValue("tacInTons_" + param_population.getName() + "_2010", tacInTons);
				log.info("tacInTons2010 = " + tacInTons);
				log.info("param_tacInTons2010 = " + param_tacInTons2010);
			}
			
            TargetSpecies ts = metier.getMetierSeasonInfo(step.getMonth()) 
                    .getSpeciesTargetSpecies(param_population.getSpecies());
					
				if (ts != null) {
                double catchTons = RuleUtil.getTotalCatchTonsPop(context, param_population, step);
				log.info("catchTons = " + catchTons);
                log.info("[TAC] catchTons = " + catchTons + " >= tacInTons:" + tacInTons);
                if (catchTons >= tacInTons) {
                    result = true;
					}
				}
			log.info("tacInTons = " + tacInTons);
		}
		return result;
	}

    /**
     * Si la condition est vrai alors cette action est executee avant le pas
     * de temps de la simulation.
     * 
     * @param context la simulation pour lequel on utilise cette regle
     * @param step le pas de temps courant
     * @param metier le metier concerne
     */
    @Override
    public void preAction(SimulationContext context, TimeStep step, Metier metier) throws Exception {
			
		//context.setValue("tacInTons2010", param_tacInTons2010);
	
        affectation = true; //true ?

        log.info("[TAC] preAction for: " + metier);
        log.info(" TAC atteint [TAC] preAction for: " + metier);
        TargetSpecies ts = metier.getMetierSeasonInfo(step.getMonth()) 
                    .getSpeciesTargetSpecies(param_population.getSpecies());
        if (ts != null && ts.isPrimaryCatch()) {
            // recupere tous les metiers qui ont l'espece en capture principale =>metiers vises
            // aimedMetiers ne fonctionne pas je ne sais pas pourquoi ! mais au final forbiddenMetier aura le meme effet
            
            context.getMetierMonitor().addforbiddenMetier(metier);

            //recupere toutes les strategies pratiquant le metier et pour lesquelles la proportion !=0
            SiMatrix siMatrix = SiMatrix.getSiMatrix(context);
            Set<Strategy> strs = new HashSet<>();
            for (Strategy str : siMatrix.getStrategies(step)) {
                double prop = str.getStrategyMonthInfo(step.getMonth())
                        .getProportionMetier(metier);
                if (prop != 0) {
                    strs.add(str);
                }
            }

            for (Strategy str : strs) {
                StrategyMonthInfo smi = str.getStrategyMonthInfo(step
                        .getMonth());

                // 1er cas de figure: l'effort est reporte sur un metier de la
                // meme strategie, n'ayant pas l'espece comme capture principale
                // et pechant avec le meme engin
                Set<Metier> possibleMetierCase1 = new HashSet<>();
                // second cas de figure: on cherche un metier de substitution
                // sans condition sur les engins, mais qui soit pratique
                Set<Metier> possibleMetierCase2 = new HashSet<>();
                // 3 eme cas de figure: on cherche des metiers non vises,
                // sans consideration sur les engins, et pour lesquels la
                // proportion peut etre nulle
                Set<Metier> possibleMetierCase3 = new HashSet<>();

                for (EffortDescription effort : str.getSetOfVessels()
                        .getPossibleMetiers()) {
                    Metier newMetier = effort.getPossibleMetiers();
                    if (
                    /*!aimedMetiers.contains(newMetier)
                    &&*/!metier.getName().equalsIgnoreCase("nonActiviy")
                            && !metier.getName().equalsIgnoreCase("nonActivie")
                            && !metier.getName().equalsIgnoreCase(
                                    "non Activite")
                            && !context.getMetierMonitor().getForbiddenMetier()
                                    .contains(newMetier)) {
                        possibleMetierCase3.add(newMetier);

                        if (smi.getProportionMetier(newMetier) != 0) {
                            possibleMetierCase2.add(newMetier);

                            if (metier.getGear().equals(newMetier.getGear())) {
                                possibleMetierCase1.add(newMetier);
                            }
                        }
                    }
                }

                Set<Metier> possibleMetier = null;
                if (possibleMetierCase1.size() != 0) {
                    log.info("[TAC] Use case 1");
                    possibleMetier = possibleMetierCase1;
                } else if (possibleMetierCase2.size() != 0) {
                    log.info("[TAC] Use case 2");
                    possibleMetier = possibleMetierCase2;
                } else if (possibleMetierCase3.size() != 0) {
                    log.info("[TAC] Use case 3");
                    possibleMetier = possibleMetierCase3;
                }

                if (possibleMetier != null) {
                    // on repartit maintenant l'effort entre les differents metiers
                    // possibles dans la meme strategie si un metier possible existe
                    // bien la repartion est proportionnelle a l'effort deja alloue
                    // dans la strategie

                    double somme = 0;
                    for (Metier met : possibleMetier) {
                        somme += smi.getProportionMetier(met);
                    }
                    for (Metier met : possibleMetier) {
                        double newProportion = smi.getProportionMetier(met)
                                + (smi.getProportionMetier(metier)
                                        * smi.getProportionMetier(met) / somme);
                        smi.setProportionMetier(met, newProportion);
                    }
                    smi.setProportionMetier(metier, 0); //le metier vise a alors une proportion nulle 
                    log.info("[TAC] il y a des metiers possibles");
                } else {
                    log.info("[TAC] Use no activity");

                    // sinon on met tout dans le metier nonActivite
                    MetierMonitor metierMon = context.getMetierMonitor();
                    MatrixND mat = metierMon.getOrCreateNoActivity(step,
                            MatrixNoActivity.NAME, siMatrix
                                    .getStrategies(step), siMatrix
                                    .getMetiers(step));
                    mat.setValue(str, metier, smi.getProportionMetier(metier));

                    smi.getProportionMetier().setValue(metier, 0);
                }
            }
        }
    }

    /**
     * Si la condition est vrai alors cette action est execute apres le pas
     * de temps de la simulation.
     * 
     * @param context La simulation pour lequel on utilise cette regle
     * @param step le pas de temps courant
     * @param metier le metier concerne
     */
    @Override
    public void postAction(SimulationContext context, TimeStep step, Metier metier)
            throws Exception {
        
        log.info("[TAC] postAction for: " + metier);
       
            if (affectation) { // sans ! ?
                // ATTENTION        
                // les captures pour cette metapop ne sont plus du qu'au metier
                // pour qui l'espece est secondaire: elles sont affectees aux
                // rejets

                //pb : ne se fait pas par metier
                // il faut une matrice pour chaques pas de temps qui stocke les
                // rejets par metier, par metapop et par classes d'age (comme
                // pour les captures)
                //////
                PopulationMonitor popMon = context.getPopulationMonitor();
                log.info("popMon biomass" + popMon.getBiomass(param_population)); // A priori on travaille ici sur une pop donnee car on defini le TAC sur une seule pop... Donc pas la peine de boucler sur les populations.
                    
					//if (!param_population.getName().equals("param_population_new")) {
                        log.info("param_population : " + param_population.getName());
                        // si on a deja une matrice rejet on le vide (elle vient
                        // forcement de la regle taille minimale or si le tac est
                        // atteint, tout va dorenavent dans les rejets et on met
                        // TOUTES les captures dans les rejets
                        MatrixND discard = popMon.getDiscard(step, param_population);
                        //log.info("discard : " + discard);
                        if (discard != null) {
                            discard.mults(0);
                        }
                        log.info("catch = " + popMon.getCatch(param_population));
                        MatrixND discardRegle = popMon.getCatch(param_population).copy(); //nouvelle instance pour le calcul dans la regle
                        // ca ne doit pas pouvoir marcher car MATRIX_DISCARDS_PER_STR_MET est de dimension pop groupe str met - et discard n'a plus la dimension pop 
                        discardRegle.setName(MatrixDiscardsPerStrMetPerZonePop.NAME);
                        popMon.addDiscard(step, param_population, discardRegle);
                        log.info("[TAC] add discard for " + param_population + ": "
                                + discardRegle);
                        if (param_propSurvie > 0) {
                            MatrixND eff = popMon.getN(param_population);
                            //on rajoute les survivants aux effectifs
                            for (MatrixIterator i = discardRegle.iterator(); i
                                    .next();) {
                                Object[] coord = i.getSemanticsCoordinates();
                                eff.setValue(coord[2], coord[3], eff.getValue(
                                        coord[2], coord[3])
                                        + i.getValue() * param_propSurvie);
                            }
                        } 
					//}	
				// on a affecte une fois cette meta pop au rejet il ne faut pas
                // le refaire
				affectation = false; // false ?
            }
		//context.setValue("tacInTons2010", param_tacInTons2010);
    }
}
