/*
 * #%L
 * IsisFish data
 * %%
 * Copyright (C) 2017 - 2018 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * #L%
 */
package rules;

import fr.ifremer.isisfish.entities.Metier;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationGroup;
import fr.ifremer.isisfish.entities.Species;
import fr.ifremer.isisfish.rule.AbstractRule;
import fr.ifremer.isisfish.simulator.PopulationMonitor;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.util.Doc;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixIterator;
import org.nuiton.math.matrix.MatrixND;
import resultinfos.MatrixDiscardsPerStrMetPerZonePop;

/**
 * Cette methode remplace aussi TailleMin pour le Merlu dans PecherieGdG
 * inspire de TailleMIN
 * Created: 30 novembre 2006
 *
 * @author anonymous <anonymous@labs.libre-entreprise.org>
 * @version $Revision: 1.1 $
 *
 * Last update: $Date: 2012 08_28 $
 * by : $Stephanie Mahevas$
 */
public class Ogive_de_Tri_Merlu extends AbstractRule {

    /** to use log facility, just put in your code: log.info("..."); */
    static private Log log = LogFactory.getLog(Ogive_de_Tri_Merlu.class);

    @Doc(value = "Begin step")
    public TimeStep param_beginStep = new TimeStep(0);

    @Doc(value = "End step")
    public TimeStep param_endStep = new TimeStep(119);
	
    @Doc(value = "Affected species")
    public Species param_species = null;

    @Doc(value = "param_Retention_L50")
    public double param_Retention_L50 = 29.4307;

    @Doc(value = "param_Retention_slope")
    public double param_Retention_slope = 2.02485;
	
	@Doc(value = "param_TailleMin")
    public double param_TailleMin = 27;

    @Doc(value = "Proportion de survie")
    public double param_propSurvie = 0;
	
	boolean affectation = true;

    public String[] necessaryResult = {
        // put here all necessary result for this rule
        // example:
        // MatrixBiomass.NAME,
        // MatrixNetValueOfLandingsPerStrategyMet.NAME
    };

    @Override
    public String[] getNecessaryResult() {
        return this.necessaryResult;
    }

    /**
     * Permet d'afficher a l'utilisateur une aide sur la regle.
     * 
     * @return L'aide ou la description de la regle
     */
    @Override
    public String getDescription() throws Exception {
        return "rejet des captures de merlu selon une sigmoide avec prop de survie selon Metier - pecherie Merlu-Langoustine GdG";
    }

    /**
      * des valeurs
     * 
     * @param context La simulation pour lequel on utilise cette regle
     */
    @Override
    public void init(SimulationContext context) throws Exception {
    }

   
    /**
     * La condition qui doit etre vrai pour faire les actions
     * 
     * @param context La simulation pour lequel on utilise cette regle
     * @return vrai si on souhaite que les actions soit faites
     */
    @Override
    public boolean condition(SimulationContext context, TimeStep step, Metier metier)
            throws Exception {
       
        boolean result = true;
        if (step.before(param_beginStep)) {
            result = false;
        } else if (step.after(param_endStep)) {
            result = false;
        }
        

        log.info("fin de condition OgiveTri:" + result);
        return result;

        // fin
    }

    /**
     * Si la condition est vrai alors cette action est execut
          * temps de la simulation.
     * 
     * @param context La simulation pour lequel on utilise cette regle
     */
    @Override
    public void preAction(SimulationContext context, TimeStep step, Metier metier)
            throws Exception {
        // nothing
	   affectation = true;
    }

    /**
     * Si la condition est vrai alors cette action est execut
          * temps de la simulation.
     * @param context La simulation pour lequel on utilise cette regle
     */
    @Override
    public void postAction(SimulationContext context, TimeStep step, Metier metier)
            throws Exception {
       
	  // Application de l'ogive de tri sur les captures  
      // la matrice de captures a une dimension metier - donc a cette etape, toutes 
	  // les captures de tous les metiers sont connues
	  // Ne doit pas s'appliquer pour chaque metier de la boucle mais une seule fois! 
	  // affectation est mis ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â  true ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â  la fin de la boucle sur les pop
		
		if (affectation) {
			log.info("deb de affecterCaptureRejetSelonOgive");

			PopulationMonitor popMon = context.getPopulationMonitor();
			
            for (Population pop : param_species.getPopulation()) {
					MatrixND discard = popMon.getCatch(pop).copy();
					MatrixND eff = popMon.getN(pop);
					
					for (MatrixIterator i = discard.iterator(); i.next();) {
                            Object[] coordonnees = i.getSemanticsCoordinates();
               				PopulationGroup group = (PopulationGroup) coordonnees[2];
							Metier met = (Metier) coordonnees[1];
							if (met.getName().equals("metier lang simp Sud") ||
                                    met.getName().equals("metier lang simp Nord") ||
                                    met.getName().equals("metier lang simple") ||
                                    met.getName().equals("metier lang jum Nord") ||
                                    met.getName().equals("metier lang jum Sud") ||
                                    met.getName().equals("metier lang jum")) {
								 
								  double propTrie = 1 / (1 + Math.exp(-(group.getLength() - param_Retention_L50)/ param_Retention_slope));
								  double propRejet = 1 - propTrie;
								  log.info("REJETS" + propRejet);
								  double value = i.getValue() * propRejet;
								  log.info("VALUE" + value);
								  i.setValue(value);
								  if (param_propSurvie >0) {
									//ajout de la survie aux effectifs
									eff.setValue(coordonnees[2], coordonnees[3],
                                        eff.getValue(coordonnees[2],
                                        coordonnees[3]) + i.getValue() *
                                        param_propSurvie);
							}
								  
							}else if (group.getLength() >= param_TailleMin) { 
								   i.setValue(0); // initialise discard si pas de rejet (ie taille>=TailleMin et pas metierLangoustine)
							} else{ // group.getLength() < param_TailleMin
						     	if (param_propSurvie >0) {
									//ajout de la survie aux effectifs
									eff.setValue(coordonnees[2], coordonnees[3],
                                        eff.getValue(coordonnees[2],
                                        coordonnees[3]) + i.getValue() *
                                        param_propSurvie);
								}
							}	
					}
					discard.setName(MatrixDiscardsPerStrMetPerZonePop.NAME);
					popMon.addDiscard(step, pop, discard); 
			}
			affectation = true; // pour ne pas refaire l'affectation au prochain metier
		}
    }
}
