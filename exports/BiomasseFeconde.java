/*
 * #%L
 * IsisFish data
 * %%
 * Copyright (C) 2006 - 2018 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * #L%
 */
package exports;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Writer;

import org.nuiton.math.matrix.*;

import resultinfos.MatrixBiomass;

import fr.ifremer.isisfish.entities.*;
import fr.ifremer.isisfish.export.Export;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.datastore.SimulationStorage;

/**
 * Biomasses.java
 *
 * Created: 23 novembre 2006
 *
 * @author anonymous <anonymous@labs.libre-entreprise.org>
 * @version $Revision:  $
 *
 * Last update: $Date: 28/08/2012$
 * by : $Author:lgasche $
 */
public class BiomasseFeconde implements Export {

    /** to use log facility, just put in your code: log.info("..."); */
    static private Log log = LogFactory.getLog(BiomasseFeconde.class);

    protected String [] necessaryResult = {
            MatrixBiomass.NAME
    };

    @Override
    public String[] getNecessaryResult() {
        return this.necessaryResult;
    }

    @Override
    public String getExportFilename() {
        return "BiomasseFeconde";
    }

    @Override
    public String getExtensionFilename() {
        return ".csv";
    }

    @Override
    public String getDescription() {
        return "Exporte les biomasses fecondes, tableau avec des lignes pop;id;zone;date;nombre";
    }

    @Override
    public void export(SimulationStorage simulation, Writer out) throws Exception {
        for (Population pop : simulation.getParameter().getPopulations()) {
            MatrixND mat = simulation.getResultStorage().getMatrix(pop, MatrixBiomass.NAME);
            for (MatrixIterator i=mat.iterator(); i.hasNext();) {
                i.next();
                Object [] sems = i.getSemanticsCoordinates();
                TimeStep step = (TimeStep)sems[0];
                PopulationGroup group = (PopulationGroup)sems[1];
				Zone zone = (Zone)sems[2];
                
                double val = i.getValue()* group.getMaturityOgive();
                out.write(pop.getName() +";"+ group.getId() +";"+ zone.getName() +";"+ step.getStep() +";"+ val +"\n");
            }
        }
    }
}
