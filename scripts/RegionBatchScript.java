/*
 * #%L
 * IsisFish data
 * %%
 * Copyright (C) 2016 Ifremer, Code Lutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * #L%
 */
package scripts;

import java.util.*;

import org.nuiton.topia.TopiaContext;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.entities.*;

/**
 * Ce script sert à modifier une region de facon informatique car il peut être très
 * couteux en temps de cliquer à de nombreuses reprises dans l'interface graphique.
 * 
 * @author Eric Chatellier
 */
public class RegionBatchScript {

    public void run() {
        // ouverture d'une region par son nom
        RegionStorage region = RegionStorage.getRegion("gdg2");
        
        // ouverture d'une transaction pour effectuer toutes les modifications
        TopiaContext tx = region.getStorage().beginTransaction();
        
        // utilisation du contexte dans d'autre methodes
        createRegionCells(tx);
        updateAllZones(tx);
        updateMetier(tx);

        // commit : valide les transactions en base de données
        // roolback : annule les modifications en base de données (permet de tester le script)
        //tx.commitTransaction();
        tx.rollbackTransaction();
        
        // close region
        tx.closeContext();
    }

    /**
     * Exemple de creation de nombreuses cellules.
     * 
     * @param tx transaction
     */
    protected void createRegionCells(TopiaContext tx) {
        CellDAO cellDAO = IsisFishDAOHelper.getCellDAO(tx);

        // creer 50 cellules
        for (int i = 1; i < 50; i++) {
            Cell cell = new CellImpl();
            cell.setName("test cells " + i);
            cell.setLatitude(5.4f);
            cell.setLongitude(2.4f);
            cellDAO.create(cell);
        }
    }

    /**
     * Mise à jour d'un paramètre de toutes les zones.
     * 
     * @param tx transaction
     */
    protected void updateAllZones(TopiaContext tx) {
        ZoneDAO zoneDAO = IsisFishDAOHelper.getZoneDAO(tx);
        CellDAO cellDAO = IsisFishDAOHelper.getCellDAO(tx);

        // modifie les zones par les cellules crées précédemment.
        for (Zone zone : zoneDAO.findAll()) {

            List<Cell> cells = Arrays.asList(
                cellDAO.findByName("test cell 1"),
                cellDAO.findByName("test cell 2"),
                cellDAO.findByName("test cell 3"),
                cellDAO.findByName("test cell 4"),
                cellDAO.findByName("test cell 5")
            );

            zone.setCell(cells);
        }
    }

    /**
     * Modifie les metiers utilisé, les equations des TargetSpecies
     * 
     * @param tx transaction
     */
    protected void updateMetier(TopiaContext tx) {
        MetierDAO metierDAO = IsisFishDAOHelper.getMetierDAO(tx);
        
        List<Metier> metiers = metierDAO.findAll();
        for (Metier metier : metiers) {
            List<MetierSeasonInfo> msis = metier.getMetierSeasonInfo();
            for (MetierSeasonInfo msi : msis) {
                Collection<TargetSpecies> stsList = msi.getSpeciesTargetSpecies();
                
                for (TargetSpecies ts : stsList) {
                    ts.getTargetFactorEquation().setContent("return 1;");
                }
            }
        }
    }

    /**
     * Point d'entrée du script.
     */
    public static void main(String[] args) {
        RegionBatchScript script = new RegionBatchScript();
        script.run();
        System.out.println("Done: " + new Date());
    }
}
