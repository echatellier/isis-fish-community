/*
 * #%L
 * IsisFish data
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * #L%
 */

package scripts;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.nuiton.topia.TopiaException;

import exports.Abundances;
import exports.Biomasses;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.simulator.*;
import fr.ifremer.isisfish.export.ExportInfo;
import fr.ifremer.isisfish.export.ExportStep;
import fr.ifremer.isisfish.export.ExportHelper;

/**
 * Ce script sert à rejouer des exports sur un grand nombre de simulation en lisant le
 * ResultStrage et en rejouant les fonctions d'export.
 * 
 * Ce script peut être lancé via l'option "Evaluer" depuis l'interface des scripts d'Isis.
 * 
 * @author Eric Chatellier
 */
public class RunExportOnManySimulationResult {

    // nom des exports a rejouer (à modifier selon le besoin)
    protected List<? extends ExportInfo> exports = Arrays.asList(
            new Abundances(),
            new Biomasses()
    );

    public void doAllExports() throws Exception {
        // prefix des noms de simulations (à configurer)
        String simulationPrefix = "as_test_";
        int simulationCount = 2000;

        // parcourt de chaque simualtion
        for (int index = 0; index <= simulationCount; index++) {
            
            // get current simulation
            String currentSimulationName = simulationPrefix + index;
            SimulationStorage currentSimulation = SimulationStorage.getSimulation(currentSimulationName);

            // temp fix for matrix semantics decorations (no opened transaction)
            SimulationContext.get().setSimulationStorage(currentSimulation);

            // run each export for this simulation
            runExports(currentSimulation, exports);
            System.out.println("Exports done on simulation " + currentSimulationName);
            
            // close all
            currentSimulation.closeStorage();
        }
    }

    protected void runExports(SimulationStorage currentSimulation, List<? extends ExportInfo> exports) throws Exception {
        File rootDirectory = currentSimulation.getDirectory();
        File exportDir = SimulationStorage.getResultExportDirectory(rootDirectory);
            
        for (ExportInfo export : exports) {
            File file = getExport(exportDir, export);
            ExportHelper.exportToFile(currentSimulation, export.getClass().getSimpleName(), file);
        }
    }

    protected File getExport(File destdir, ExportInfo exportInfo) {
        String filename = exportInfo.getExportFilename();
        String extension = exportInfo.getExtensionFilename();
        
        File file = new File(destdir, filename + extension);
        // prevent two export with same name
        // name MyExport.csv become MyExport_1.csv
        int val = 0;
        while (file.exists()) {
            val++;
            file = new File(destdir, filename + extension + "_" + val);
        }
        return file;
    }

    public static void main(String[] args) throws Exception {
        RunExportOnManySimulationResult runExportJob = new RunExportOnManySimulationResult();
        runExportJob.doAllExports();
    }
}
