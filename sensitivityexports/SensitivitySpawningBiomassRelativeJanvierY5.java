/*
 * #%L
 * IsisFish data
 * %%
 * Copyright (C) 2009 - 2018 Ifremer, Code Lutin, Jean Couteau, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * #L%
 */
package sensitivityexports;

import fr.ifremer.isisfish.datastore.ResultStorage;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationGroup;
import fr.ifremer.isisfish.export.SensitivityExport;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.util.Doc;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixIterator;
import org.nuiton.math.matrix.MatrixND;

import resultinfos.MatrixBiomass;

import java.io.Writer;

public class SensitivitySpawningBiomassRelativeJanvierY5 implements SensitivityExport {

    /** to use log facility, just put in your code: log.info("..."); */
    static private Log log = LogFactory.getLog(SensitivitySpawningBiomassRelativeJanvierY5.class);

    protected String[] necessaryResult = {
            MatrixBiomass.NAME
    };

    @Doc("Population")
    public Population param_pop;

    @Override
    public void export(SimulationStorage simulation, Writer out)
            throws Exception {
        TimeStep lastStep = simulation.getResultStorage().getLastStep();
		TimeStep janvierLastYear = new TimeStep(12 * lastStep.getYear());
        TimeStep firstStep = new TimeStep(0);
        double biomass = 0.0;
        double firstbiomass = 0.0;

        for (Population pop : simulation.getParameter().getPopulations()) {
            if (pop.getName().equals(param_pop.getName())) {
                ResultStorage resultStorage = simulation.getResultStorage();

                //Get the biomass of the first time step
                MatrixND matfirstdate = resultStorage.getMatrix(pop, MatrixBiomass.NAME);
                for (MatrixIterator i = matfirstdate.iterator(); i.hasNext();) {
                    i.next();
                    Object[] sems = i.getSemanticsCoordinates();
                    PopulationGroup group = (PopulationGroup) sems[1];
                    TimeStep step = (TimeStep) sems[0];
                    if (step.equals(firstStep)) {
                        firstbiomass += i.getValue()
                                * group.getMaturityOgive();
                    }
                }

                //Get the biomass of the last time step
                MatrixND matlastJan = resultStorage.getMatrix(pop, MatrixBiomass.NAME);
                for (MatrixIterator i = matlastJan.iterator(); i.hasNext();) {
                    i.next();
                    Object[] sems = i.getSemanticsCoordinates();
                    PopulationGroup group = (PopulationGroup) sems[1];
                    TimeStep step = (TimeStep) sems[0];
                    if (step.equals(janvierLastYear))
                        biomass += i.getValue() * group.getMaturityOgive();
                }
            }
        }
        out.write(Double.toString(biomass / firstbiomass));
    }

    @Override
    public String getDescription() {
        return "Biomass of genitors January last year / Biomass of genitors January first year . Biomass is the sum on the groups and zones";
    }

    @Override
    public String getExportFilename() {
        return "SensitivityGenitorBiomassRelativeJanvierY5_" + param_pop.getName();
    }

    @Override
    public String getExtensionFilename() {
        return ".csv";
    }

    @Override
    public String[] getNecessaryResult() {
        return this.necessaryResult;
    }

}
