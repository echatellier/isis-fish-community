/*
 * #%L
 * IsisFish data
 * %%
 * Copyright (C) 2009 - 2011 Ifremer, Code Lutin, Jean Couteau
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * #L%
 */
package sensitivityexports;

import java.io.Writer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixND;

import resultinfos.MatrixBiomass;
import fr.ifremer.isisfish.datastore.ResultStorage;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.export.SensitivityExport;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.util.Doc;

public class SensitivityBiomassRelativeJanvierY6 implements SensitivityExport {

    /** to use log facility, just put in your code: log.info("..."); */
    static private Log log = LogFactory
            .getLog(SensitivityBiomassRelativeJanvierY6.class);

    protected String[] necessaryResult = {
            MatrixBiomass.NAME
    };

    @Doc("Population")
    public Population param_pop;

    @Override
    public void export(SimulationStorage simulation, Writer out)
            throws Exception {
        ResultStorage resultStorage = simulation.getResultStorage();
        TimeStep lastStep = resultStorage.getLastStep();
		TimeStep janvierLastYear = new TimeStep(12 * lastStep.getYear()); 
        TimeStep firstStep = new TimeStep(0);
        double biomass = 0.0;
        double firstbiomass = 0.0;

        for (Population pop : simulation.getParameter().getPopulations()) {
            if (pop.getName().equals(param_pop.getName())) {                

                //Get the biomass on december of the first year
                MatrixND matfirstdate = resultStorage.getMatrix(firstStep, pop, MatrixBiomass.NAME);
                firstbiomass = matfirstdate.sumAll();

                //Get the biomass on the month of January of the last time step
                MatrixND matlastJan = resultStorage.getMatrix(janvierLastYear, pop, MatrixBiomass.NAME);
                biomass = matlastJan.sumAll();

            }
        }
        out.write(Double.toString(biomass / firstbiomass));
    }

    @Override
    public String getDescription() {
        return "Biomass January last year / Biomass January first year. Biomass is the sum on the groups and zones";
    }

    @Override
    public String getExportFilename() {
        return "SensitivityBiomassRelativeJanvierY6_" + param_pop.getName();
    }

    @Override
    public String getExtensionFilename() {
        return ".csv";
    }

    @Override
    public String[] getNecessaryResult() {
        return this.necessaryResult;
    }

}
