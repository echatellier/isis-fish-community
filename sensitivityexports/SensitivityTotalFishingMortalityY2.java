/*
 * #%L
 * IsisFish data
 * %%
 * Copyright (C) 2009 - 2018 Ifremer, Code Lutin, Jean Couteau
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * #L%
 */
package sensitivityexports;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Writer;

import org.nuiton.math.matrix.*;

import resultinfos.MatrixTotalFishingMortality;

import fr.ifremer.isisfish.datastore.ResultStorage;
import fr.ifremer.isisfish.datastore.SimulationStorage;

import fr.ifremer.isisfish.entities.*;

import fr.ifremer.isisfish.export.SensitivityExport;

import fr.ifremer.isisfish.types.TimeStep;

import fr.ifremer.isisfish.util.Doc;

public class SensitivityTotalFishingMortalityY2 implements SensitivityExport {

    /** to use log facility, just put in your code: log.info("..."); */
    static private Log log = LogFactory.getLog(SensitivityTotalFishingMortalityY2.class);

    protected String[] necessaryResult = {MatrixTotalFishingMortality.NAME};

    @Doc("Population")
    public Population param_pop;

    @Override
    public void export(SimulationStorage simulation, Writer out) throws Exception {
        ResultStorage resultStorage = simulation.getResultStorage();
        TimeStep lastStep = resultStorage.getLastStep();
        TimeStep firstStep = new TimeStep(11);
        double firsttotalFishingMortality = 0.0;
		double totalFishingMortality = 0.0;

        for (Population pop : simulation.getParameter().getPopulations()) {
            if (pop.getName().equals(param_pop.getName())) {
                //Get the fishing mortality on december of the first year
                MatrixND matfirstdate = resultStorage.getMatrix(firstStep, pop, MatrixTotalFishingMortality.NAME);
                firsttotalFishingMortality = matfirstdate.sumAll();

                //Get the fishing mortality of the (last step of) the last year.
                MatrixND matlastdate = resultStorage.getMatrix(lastStep, pop, MatrixTotalFishingMortality.NAME);
                totalFishingMortality = matlastdate.sumAll();
            }
        }

        out.write(Double.toString(totalFishingMortality / firsttotalFishingMortality));
    }

    @Override
    public String getDescription() {
        return "Total fishing mortality for the last year for the population. Total fishing mortality is the mean on zones, representative groups, and the sum of metiers and strategies";
    }

    @Override
    public String getExportFilename() {
        return "SensitivityTotalFishingMortalityY2_" + param_pop.getName();
    }

    @Override
    public String getExtensionFilename() {
        return ".csv";
    }

    @Override
    public String[] getNecessaryResult() {
        return this.necessaryResult;
    }

}
